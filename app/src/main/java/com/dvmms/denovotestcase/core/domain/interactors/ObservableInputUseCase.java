package com.dvmms.denovotestcase.core.domain.interactors;

import com.dvmms.denovotestcase.domain.executors.PostExecutionThread;
import com.dvmms.denovotestcase.domain.executors.ThreadExecutor;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.schedulers.Schedulers;
import rx.subscriptions.Subscriptions;

/**
 * Created by pmalyugin on 13/03/2018.
 */

public abstract class ObservableInputUseCase<Input, Output> {
    private final ThreadExecutor threadExecutor;
    private final PostExecutionThread postExecutionThread;
    private boolean isExecuting;

    private Subscription subscription = Subscriptions.empty();

    protected ObservableInputUseCase(
            ThreadExecutor threadExecutor,
            PostExecutionThread postExecutionThread
    ) {
        this.threadExecutor = threadExecutor;
        this.postExecutionThread = postExecutionThread;
        this.isExecuting = false;
    }

    public boolean isExecuting() {
        return isExecuting;
    }

    /**
     * Builds an {@link rx.Observable} which will be used when executing the current {@link ObservableUseCase}.
     */
    protected abstract Observable<Output> buildUseCaseObservable(Input input);

    /**
     * Executes the current use case.
     *
     * @param UseCaseSubscriber The guy who will be listen to the observable build with {@link #buildUseCaseObservable}.
     */
    public synchronized void execute(Subscriber<Output> UseCaseSubscriber, Input input) {
        isExecuting = true;
        this.subscription = this.buildUseCaseObservable(input)
                .subscribeOn(Schedulers.from(threadExecutor))
                .observeOn(postExecutionThread.getScheduler())
                .doOnError(throwable -> isExecuting = false)
                .doOnCompleted(() -> isExecuting = false)
                .doOnUnsubscribe(() -> isExecuting = false)
                .subscribe(UseCaseSubscriber)
        ;
    }

    /**
     * Unsubscribes from current {@link rx.Subscription}.
     */
    public synchronized void unsubscribe() {
        if (!isUnsubscribed()) {
            subscription.unsubscribe();
        }
    }

    public synchronized boolean isUnsubscribed() {
        return (subscription == null || subscription.isUnsubscribed());
    }
}
