package com.dvmms.denovotestcase.core.di.modules;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;

import com.dvmms.denovotestcase.core.di.PerActivity;

import dagger.Module;
import dagger.Provides;

/**
 * Created by pmalyugin on 13/03/2018.
 */
@Module
public class ActivityModule {
    private final Activity activity;

    public ActivityModule(Activity activity) {
        this.activity = activity;
    }

    /**
     * Expose the activity to dependents in the graph.
     */
    @Provides
    @PerActivity
    Activity activity() {
        return this.activity;
    }
}
