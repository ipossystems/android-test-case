package com.dvmms.denovotestcase.core.ui;

import android.os.Bundle;

import com.arellomobile.mvp.MvpAppCompatDialogFragment;
import com.dvmms.denovotestcase.core.di.BaseDepends;
import com.dvmms.denovotestcase.core.di.FragmentComponent;
import com.dvmms.denovotestcase.core.di.HasComponent;

/**
 * Created by pmalyugin on 13/03/2018.
 */

public abstract class MvpBaseFragment <T extends FragmentComponent, D extends BaseDepends>
        extends MvpAppCompatDialogFragment
{
    private T component;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        D depends = getDepends();
        component = buildComponent(depends);
        setupDI(component);
        super.onCreate(savedInstanceState);
    }

    protected abstract T buildComponent(D depends);
    protected abstract void setupDI(T component);

    /**
     * Gets a component for dependency injection by its type.
     */
    @SuppressWarnings("unchecked")
    protected D getDepends() {
        HasComponent<D> hasComponent = (HasComponent<D>)getActivity();
        return hasComponent.getComponent();
    }
}