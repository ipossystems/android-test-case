package com.dvmms.denovotestcase.core.di;

import java.lang.annotation.Retention;
import javax.inject.Scope;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by pmalyugin on 13/03/2018.
 */

@Scope
@Retention(RUNTIME)
public @interface PerActivity {}