package com.dvmms.denovotestcase.core.domain.executors;

import com.dvmms.denovotestcase.domain.executors.PostExecutionThread;

import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by pmalyugin on 13/03/2018.
 */

public class UiThread implements PostExecutionThread {
    @Override
    public Scheduler getScheduler() {
        return AndroidSchedulers.mainThread();
    }
}
