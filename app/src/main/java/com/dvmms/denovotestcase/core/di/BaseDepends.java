package com.dvmms.denovotestcase.core.di;

import com.dvmms.denovotestcase.domain.executors.PostExecutionThread;
import com.dvmms.denovotestcase.domain.executors.ThreadExecutor;

/**
 * Created by pmalyugin on 13/03/2018.
 */

public interface BaseDepends {
    ThreadExecutor threadExecutor();
    PostExecutionThread postExecutionThread();
}
