package com.dvmms.denovotestcase.core.ui;

import android.os.Bundle;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.dvmms.denovotestcase.application.ConsumerApp;
import com.dvmms.denovotestcase.application.di.ApplicationComponent;
import com.dvmms.denovotestcase.core.di.ActivityComponent;
import com.dvmms.denovotestcase.core.di.modules.ActivityModule;
import com.dvmms.denovotestcase.core.di.HasComponent;

/**
 * Created by pmalyugin on 13/03/2018.
 */

public abstract class MvpBaseActivity<C extends ActivityComponent>
        extends MvpAppCompatActivity
        implements HasComponent<C> {

    private C component;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        component = buildComponent();
        setupDI(component);
        super.onCreate(savedInstanceState);
    }

    protected ApplicationComponent getApplicationComponent() {
        return ConsumerApp.getAppComponent();
    }

    protected ActivityModule getActivityModule() {
        return new ActivityModule(this);
    }

    @Override
    public C getComponent() {
        return component;
    }

    //region DI
    protected abstract C buildComponent();
    protected abstract void setupDI(C component);
    //endregion
}