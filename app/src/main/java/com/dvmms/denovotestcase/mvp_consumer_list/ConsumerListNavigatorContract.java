package com.dvmms.denovotestcase.mvp_consumer_list;

import com.dvmms.denovotestcase.domain.models.Consumer;

/**
 * Created by pmalyugin on 13/03/2018.
 */

public interface ConsumerListNavigatorContract {
    void editConsumer(Consumer consumer);
}
