package com.dvmms.denovotestcase.mvp_consumer_list.di;

import com.dvmms.denovotestcase.core.di.BaseDepends;
import com.dvmms.denovotestcase.core.di.FragmentComponent;
import com.dvmms.denovotestcase.core.di.PerFragment;
import com.dvmms.denovotestcase.domain.repositories.ConsumerRepositoryContract;
import com.dvmms.denovotestcase.mvp_consumer_list.ConsumerListNavigatorContract;
import com.dvmms.denovotestcase.mvp_consumer_list.ui.ConsumerListFragment;

import dagger.Component;

/**
 * Created by pmalyugin on 13/03/2018.
 */
@PerFragment
@Component(
        dependencies = ConsumerListFrComponent.Depends.class
)
public interface ConsumerListFrComponent extends FragmentComponent {
    void inject(ConsumerListFragment fragment);

    interface Depends extends BaseDepends {
        ConsumerListNavigatorContract consumerListNavigator();
        ConsumerRepositoryContract consumerRepository();
    }
}
