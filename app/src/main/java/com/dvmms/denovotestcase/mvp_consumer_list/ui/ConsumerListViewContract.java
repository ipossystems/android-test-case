package com.dvmms.denovotestcase.mvp_consumer_list.ui;

import com.arellomobile.mvp.MvpView;
import com.dvmms.denovotestcase.domain.models.Consumer;

import java.util.List;

/**
 * Created by pmalyugin on 13/03/2018.
 */

public interface ConsumerListViewContract extends MvpView {
    void setConsumer(List<Consumer> consumers);
}
