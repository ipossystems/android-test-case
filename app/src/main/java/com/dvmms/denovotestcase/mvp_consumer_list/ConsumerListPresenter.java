package com.dvmms.denovotestcase.mvp_consumer_list;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.dvmms.denovotestcase.domain.interactors.GetConsumerListUseCase;
import com.dvmms.denovotestcase.domain.models.Consumer;
import com.dvmms.denovotestcase.mvp_consumer_list.ui.ConsumerListViewContract;

import java.util.List;

import javax.inject.Inject;

import rx.Subscriber;

/**
 * Created by pmalyugin on 13/03/2018.
 */
@InjectViewState
public class ConsumerListPresenter
        extends MvpPresenter<ConsumerListViewContract>
        implements ConsumerListPresenterContract
{

    private final GetConsumerListUseCase getConsumerListUseCase;
    private final ConsumerListNavigatorContract navigator;

    @Inject
    public ConsumerListPresenter(
            GetConsumerListUseCase getConsumerListUseCase,
            ConsumerListNavigatorContract navigator
    ) {
        this.getConsumerListUseCase = getConsumerListUseCase;
        this.navigator = navigator;
    }

    //region ConsumerListPresenterContract
    @Override
    public void loadConsumers() {
        getConsumerListUseCase.execute(new Subscriber<List<Consumer>>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(List<Consumer> consumers) {
                getViewState().setConsumer(consumers);
            }
        });
    }

    @Override
    public void search(String query) {

    }

    @Override
    public void edit(Consumer consumer) {

    }

    @Override
    public void create() {

    }
    //endregion
}
