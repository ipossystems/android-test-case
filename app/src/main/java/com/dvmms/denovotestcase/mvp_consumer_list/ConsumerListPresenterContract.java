package com.dvmms.denovotestcase.mvp_consumer_list;

import com.dvmms.denovotestcase.domain.models.Consumer;

/**
 * Created by pmalyugin on 13/03/2018.
 */

public interface ConsumerListPresenterContract {
    void loadConsumers();
    void search(String query);
    void edit(Consumer consumer);
    void create();
}
