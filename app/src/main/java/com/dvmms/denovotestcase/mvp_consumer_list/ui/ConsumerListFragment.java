package com.dvmms.denovotestcase.mvp_consumer_list.ui;


import android.os.Bundle;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.dvmms.denovotestcase.core.ui.MvpBaseFragment;
import com.dvmms.denovotestcase.domain.models.Consumer;
import com.dvmms.denovotestcase.mvp_consumer_list.ConsumerListPresenter;
import com.dvmms.denovotestcase.mvp_consumer_list.di.ConsumerListFrComponent;
import com.dvmms.denovotestcase.mvp_consumer_list.di.DaggerConsumerListFrComponent;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by pmalyugin on 13/03/2018.
 */

public class ConsumerListFragment
        extends MvpBaseFragment<ConsumerListFrComponent, ConsumerListFrComponent.Depends>
    implements ConsumerListViewContract
{

    @Inject
    @InjectPresenter
    ConsumerListPresenter presenter;

    @ProvidePresenter
    ConsumerListPresenter provideConsumerListPresenter() {
        return presenter;
    }

    //region ConsumerListViewContract
    @Override
    public void setConsumer(List<Consumer> consumers) {

    }
    //endregion

    //region DI
    @Override
    protected ConsumerListFrComponent buildComponent(ConsumerListFrComponent.Depends depends) {
        return DaggerConsumerListFrComponent.builder()
                .depends(depends)
                .build()
                ;
    }

    @Override
    protected void setupDI(ConsumerListFrComponent component) {
        component.inject(this);
    }
    //endregion

    public static ConsumerListFragment newInstance() {
        ConsumerListFragment fr = new ConsumerListFragment();

        Bundle args = new Bundle();
        fr.setArguments(args);

        return fr;
    }
}
