package com.dvmms.denovotestcase.mvp_pincode;

/**
 * Created by pmalyugin on 13/03/2018.
 */

public interface PinCodePresenterContract {
    void enterCode(String code);
}
