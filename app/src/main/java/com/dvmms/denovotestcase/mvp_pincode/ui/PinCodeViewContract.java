package com.dvmms.denovotestcase.mvp_pincode.ui;

import com.arellomobile.mvp.MvpView;

/**
 * Created by pmalyugin on 13/03/2018.
 */

public interface PinCodeViewContract extends MvpView {
    void showError(String error);
}
