package com.dvmms.denovotestcase.application;

import android.content.Context;
import android.content.Intent;

import com.dvmms.denovotestcase.application.di.DaggerPinCodeActComponent;
import com.dvmms.denovotestcase.application.di.PinCodeActComponent;
import com.dvmms.denovotestcase.core.ui.MvpBaseActivity;

/**
 * Created by pmalyugin on 13/03/2018.
 */

public class PinCodeActivity extends MvpBaseActivity<PinCodeActComponent> {

    @Override
    protected PinCodeActComponent buildComponent() {
        return DaggerPinCodeActComponent.builder()
                .applicationComponent(getApplicationComponent())
                .activityModule(getActivityModule())
                .build()
                ;
    }

    @Override
    protected void setupDI(PinCodeActComponent component) {
        component.inject(this);
    }

    public static Intent getCallingIntent(Context context) {
        return new Intent(context, PinCodeActivity.class);
    }
}
