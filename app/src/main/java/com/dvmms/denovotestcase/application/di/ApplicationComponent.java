package com.dvmms.denovotestcase.application.di;

import com.dvmms.denovotestcase.application.di.modules.ExecutorsModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by pmalyugin on 13/03/2018.
 */
@Singleton
@Component(
        modules = {
                ExecutorsModule.class
        }
)
public interface ApplicationComponent {
}
