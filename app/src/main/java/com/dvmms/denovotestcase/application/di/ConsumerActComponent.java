package com.dvmms.denovotestcase.application.di;

import com.dvmms.denovotestcase.application.ConsumerActivity;
import com.dvmms.denovotestcase.core.di.ActivityComponent;
import com.dvmms.denovotestcase.core.di.PerActivity;
import com.dvmms.denovotestcase.core.di.modules.ActivityModule;

import dagger.Component;

/**
 * Created by pmalyugin on 13/03/2018.
 */
@PerActivity
@Component(
        dependencies = ApplicationComponent.class,
        modules = {
                ActivityModule.class
        }
)
public interface ConsumerActComponent extends ActivityComponent {
    void inject(ConsumerActivity activity);
}
