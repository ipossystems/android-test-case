package com.dvmms.denovotestcase.application.di.modules;

import com.dvmms.denovotestcase.core.domain.executors.JobExecutor;
import com.dvmms.denovotestcase.core.domain.executors.UiThread;
import com.dvmms.denovotestcase.domain.executors.PostExecutionThread;
import com.dvmms.denovotestcase.domain.executors.ThreadExecutor;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by pmalyugin on 13/03/2018.
 */
@Module
public class ExecutorsModule {
    @Singleton
    @Provides
    ThreadExecutor provideThreadExecutor() {
        return new JobExecutor();
    }

    @Singleton
    @Provides
    PostExecutionThread providePostExecutionThread() {
        return new UiThread();
    }
}
