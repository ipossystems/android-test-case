package com.dvmms.denovotestcase.application;

import android.content.Context;
import android.content.Intent;

import com.dvmms.denovotestcase.application.di.ConsumerActComponent;
import com.dvmms.denovotestcase.application.di.DaggerConsumerActComponent;
import com.dvmms.denovotestcase.core.ui.MvpBaseActivity;
import com.dvmms.denovotestcase.domain.models.Consumer;
import com.dvmms.denovotestcase.mvp_consumer_details.ConsumerDetailsNavigatorContract;
import com.dvmms.denovotestcase.mvp_consumer_list.ConsumerListNavigatorContract;

/**
 * Created by pmalyugin on 13/03/2018.
 */

public class ConsumerActivity
        extends MvpBaseActivity<ConsumerActComponent>
        implements ConsumerListNavigatorContract,
        ConsumerDetailsNavigatorContract
{

    //region ConsumerListNavigatorContract
    @Override
    public void editConsumer(Consumer consumer) {

    }
    //endregion

    //region ConsumerDetailsNavigatorContract
    @Override
    public void onConsumerSaved(Consumer consumer) {

    }
    //endregion

    //region DI
    @Override
    protected ConsumerActComponent buildComponent() {
        return DaggerConsumerActComponent.builder()
                .applicationComponent(getApplicationComponent())
                .activityModule(getActivityModule())
                .build()
                ;
    }

    @Override
    protected void setupDI(ConsumerActComponent component) {
        component.inject(this);
    }
    //endregion

    public static Intent getCallingIntent(Context context) {
        return new Intent(context, ConsumerActivity.class);
    }
}
