package com.dvmms.denovotestcase.application;

import android.app.Application;

import com.dvmms.denovotestcase.application.di.ApplicationComponent;
import com.dvmms.denovotestcase.application.di.DaggerApplicationComponent;

/**
 * Created by pmalyugin on 13/03/2018.
 */

public class ConsumerApp extends Application {
    private static ApplicationComponent sAppComponent;


    @Override
    public void onCreate() {
        super.onCreate();
        sAppComponent = DaggerApplicationComponent.builder().build();
    }

    public static ApplicationComponent getAppComponent() {
        return sAppComponent;
    }
}
