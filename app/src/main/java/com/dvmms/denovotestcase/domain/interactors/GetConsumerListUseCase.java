package com.dvmms.denovotestcase.domain.interactors;

import com.dvmms.denovotestcase.core.domain.interactors.ObservableUseCase;
import com.dvmms.denovotestcase.domain.repositories.ConsumerRepositoryContract;
import com.dvmms.denovotestcase.domain.executors.PostExecutionThread;
import com.dvmms.denovotestcase.domain.executors.ThreadExecutor;
import com.dvmms.denovotestcase.domain.models.Consumer;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by pmalyugin on 13/03/2018.
 */

public class GetConsumerListUseCase
        extends ObservableUseCase<List<Consumer>> {

    private final ConsumerRepositoryContract consumerRepository;

    @Inject
    public GetConsumerListUseCase(
            ThreadExecutor threadExecutor,
            PostExecutionThread postExecutionThread,
            ConsumerRepositoryContract consumerRepository
    ) {
        super(threadExecutor, postExecutionThread);
        this.consumerRepository = consumerRepository;
    }

    @Override
    protected Observable<List<Consumer>> buildUseCaseObservable() {
        return consumerRepository.getAll();
    }
}
