package com.dvmms.denovotestcase.domain.repositories;

import com.dvmms.denovotestcase.domain.models.Consumer;

import java.util.List;

import rx.Observable;

/**
 * Created by pmalyugin on 13/03/2018.
 */

public interface ConsumerRepositoryContract {
    Observable<List<Consumer>> getAll();
    Observable<Consumer> getById(long id);
    Observable<Boolean> save(Consumer consumer);
}
