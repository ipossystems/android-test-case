package com.dvmms.denovotestcase.domain.executors;

import java.util.concurrent.Executor;

/**
 * Created by pmalyugin on 13/03/2018.
 */

public interface ThreadExecutor extends Executor {
}
