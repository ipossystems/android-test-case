package com.dvmms.denovotestcase.domain.models;

/**
 * Created by pmalyugin on 13/03/2018.
 */

public class Consumer {
    private long id;
    private String firstName;
    private String lastName;
    private String phone;
    private String email;

    public Consumer() {

    }

    public long getId() {
        return id;
    }

    public Consumer setId(long id) {
        this.id = id;
        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public Consumer setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public Consumer setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public String getPhone() {
        return phone;
    }

    public Consumer setPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public Consumer setEmail(String email) {
        this.email = email;
        return this;
    }
}
