package com.dvmms.denovotestcase.domain.interactors;

import com.dvmms.denovotestcase.core.domain.interactors.ObservableInputUseCase;
import com.dvmms.denovotestcase.domain.executors.PostExecutionThread;
import com.dvmms.denovotestcase.domain.executors.ThreadExecutor;
import com.dvmms.denovotestcase.domain.models.Consumer;
import com.dvmms.denovotestcase.exceptions.NotImplementedException;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by pmalyugin on 13/03/2018.
 */

public class RemoveConsumerUseCase
        extends ObservableInputUseCase<Consumer, Consumer> {

    @Inject
    public RemoveConsumerUseCase(ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
    }

    @Override
    protected Observable<Consumer> buildUseCaseObservable(Consumer consumer) {
        return Observable.error(new NotImplementedException());
    }
}
