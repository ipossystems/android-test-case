package com.dvmms.denovotestcase.mvp_consumer_details;

/**
 * Created by pmalyugin on 13/03/2018.
 */

public interface ConsumerDetailsPresenterContract {
    void load();
    void save();
    void remove();
}
