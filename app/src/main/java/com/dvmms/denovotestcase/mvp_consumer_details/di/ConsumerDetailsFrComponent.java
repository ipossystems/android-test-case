package com.dvmms.denovotestcase.mvp_consumer_details.di;

import com.dvmms.denovotestcase.core.di.BaseDepends;

import dagger.Component;

/**
 * Created by pmalyugin on 13/03/2018.
 */
@Component(
        dependencies = ConsumerDetailsFrComponent.Depends.class
)
public interface ConsumerDetailsFrComponent {

    interface Depends extends BaseDepends {

    }
}
