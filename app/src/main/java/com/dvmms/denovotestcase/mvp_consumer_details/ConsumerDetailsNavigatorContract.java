package com.dvmms.denovotestcase.mvp_consumer_details;

import com.dvmms.denovotestcase.domain.models.Consumer;

/**
 * Created by pmalyugin on 13/03/2018.
 */

public interface ConsumerDetailsNavigatorContract {
    void onConsumerSaved(Consumer consumer);
}
