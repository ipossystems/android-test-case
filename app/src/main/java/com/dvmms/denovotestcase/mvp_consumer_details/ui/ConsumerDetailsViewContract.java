package com.dvmms.denovotestcase.mvp_consumer_details.ui;

import com.arellomobile.mvp.MvpView;
import com.dvmms.denovotestcase.domain.models.Consumer;

/**
 * Created by pmalyugin on 13/03/2018.
 */

public interface ConsumerDetailsViewContract extends MvpView {
    void setConsumer(Consumer consumer);
}
