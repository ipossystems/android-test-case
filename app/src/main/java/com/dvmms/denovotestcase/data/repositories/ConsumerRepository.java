package com.dvmms.denovotestcase.data.repositories;

import com.dvmms.denovotestcase.domain.models.Consumer;
import com.dvmms.denovotestcase.domain.repositories.ConsumerRepositoryContract;
import com.dvmms.denovotestcase.exceptions.NotImplementedException;

import java.util.List;

import rx.Observable;

/**
 * Created by pmalyugin on 13/03/2018.
 */

public class ConsumerRepository implements ConsumerRepositoryContract {
    @Override
    public Observable<List<Consumer>> getAll() {
        return Observable.error(new NotImplementedException());
    }

    @Override
    public Observable<Consumer> getById(long id) {
        return Observable.error(new NotImplementedException());
    }

    @Override
    public Observable<Boolean> save(Consumer consumer) {
        return Observable.error(new NotImplementedException());
    }
}
